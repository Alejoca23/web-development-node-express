const http = require('http')
const port = process.env.port || 3000

let body = ''
const server =
    http.createServer((req, res) => {
        //normilize url by removing querystring, optional trailing slash and making it lowercase
        const path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase()

        switch (path) {
            case '':
                body = 'Homepage'
                res.writeHead(200, {
                    'Content-Length': Buffer.byteLength(body),
                    'Content-Type': 'application/json',
                }).end(body)
                break
            case '/about':
                body = 'About'
                res.writeHead(200, {
                    'Content-Length': Buffer.byteLength(body),
                    'Content-Type': 'application/json',
                }).end(body)
                break

            default:
                body = 'Page was not found'
                res.writeHead(404, {
                    'Content-Length': Buffer.byteLength(body),
                    'Content-Type': 'application/json',
                }).end(body)
                break

        }


    }
    )


server.listen(port, () =>
    console.log(`Server started on port ${port}; 
    \nPress Ctrl+C to terminate...`))