const http = require('http')
const fs = require('fs')
const port = process.env.port || 3000
let body = ''


function serverStaticFile(res, path, contentType, responseCode = 200) {
    //console.log(__dirname); // Get full path folder
    fs.readFile(__dirname + path, (err, data) => {
        if (err) {
            res.writeHead(500, {
                'Content-Type': 'text/html',
            }).end('500 - Internal Server Error :v')
        }
        res.writeHead(responseCode, {
            'Content-Length': Buffer.byteLength(data),
            'Content-Type': contentType,
        }).end(data)

    })
}


const server =
    http.createServer((req, res) => {
        //normilize url by removing querystring, optional trailing slash and making it lowercase
        const path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase()

        switch (path) {
            case '':
                serverStaticFile(res, '/public/home.html', 'text/html')
                break
            case '/about':
                serverStaticFile(res, '/public/about.html', 'text/html')
                break
            case '/img/killua.jpeg':
                serverStaticFile(res, '/public/img/killua.jpeg', 'image/jpeg')
                break

            default:
                serverStaticFile(res, '/public/404.html', 'text/html', 404)
                break

        }


    })


server.listen(port, () =>
    console.log(`Server started on port ${port}; 
    \nPress Ctrl+C to terminate...`))