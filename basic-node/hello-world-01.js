const http = require('http')
const port = process.env.port || 3000


const body = 'Hello World!'
const server = http.createServer(
    (req, res) => {
        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(body),
            'Content-Type': 'application/json',
        }).end(body)
    }
)


server.listen(port, () =>
    console.log(`Server started on port ${port}; 
    \nPress Ctrl+C to terminate...`))