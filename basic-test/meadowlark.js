const handlers = require('./lib/handlers')
const express = require('express')
const expressHandlebars = require('express-handlebars')
const port = process.env.port || 3000


const app = express()
// configure Handlebars view engine
console.log(__dirname)
app.engine('handlebars', expressHandlebars.engine({
    layoutsDir: __dirname + '/views/layouts',
    defaultLayout: 'main',
}))

app.set('views', __dirname + '/views/layouts')
app.set('view engine', 'handlebars')
app.use(express.static(__dirname + '/public'))


app.get('/', handlers.home)

app.get('/about', handlers.about)


//404 page
app.use(handlers.notFound)

//500 page
app.use(handlers.serverError)


if (require.main == module) {

    app.listen(port, () =>
        console.log(`Express started on http://localhost:${port}; 
        \nPress Ctrl+C to terminate...`))
}
else {
    module.exports = app
}
