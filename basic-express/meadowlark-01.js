const express = require('express')
const app = express()
const port = process.env.port || 3000

//404 page
app.use((req, res) => {
    res.type('application/json').status(404).send({ message: '404 - Not found' })
})

//500 page
app.use((err, req, res, next) => {
    console.error(err.message)
    res.type('application/json').status(500).send({ message: '500 - Server error' })
})





app.listen(port, () =>
    console.log(`Express started on http://localhost:${port}; 
    \nPress Ctrl+C to terminate...`))