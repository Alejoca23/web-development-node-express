const fortune = require('./lib/fortune')
const express = require('express')
const expressHandlebars = require('express-handlebars')
const port = process.env.port || 3000

const app = express()
// configure Handlebars view engine
console.log(__dirname)
app.engine('handlebars', expressHandlebars.engine({
    layoutsDir: __dirname + '/views/layouts',
    defaultLayout: 'main',
}))

app.set('views', __dirname + '/views/layouts')
app.set('view engine', 'handlebars')
app.use(express.static(__dirname + '/public'))


app.get('/', (req, res) => {
    res.render('home')
})

app.get('/about', (req, res) => {
    // res.type('application/json').send({ message: 'About Meadowlark Travel' }) //200 OK (Default status code)
    res.render('about', { fortune: fortune.randomFortune() })
})


//404 page
app.use((req, res) => {
    res.status(404).render('404')
})

//500 page
app.use((err, req, res, next) => {
    console.error(err.message)
    res.status(500).render('500')
})



app.listen(port, () =>
    console.log(`Express started on http://localhost:${port}; 
    \nPress Ctrl+C to terminate...`))